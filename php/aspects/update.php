<?php

require_once '../conn.php';
require_once 'read.php';

$id = $_POST['id'];
$title = $_POST['title'];
$range = json_decode($_POST["range"], true);

$sqlUpdateAspects = "UPDATE aspects SET title = '{$title}' WHERE id = '{$id}' ";
if (mysqli_query($conn, $sqlUpdateAspects)) {
  $response;


  for ($i = 0; $i < count($range); $i++) {
    $sql = "UPDATE aspectsranges SET min = {$range[$i]["min"]}, max = {$range[$i]["max"]} WHERE idaspect = '{$id}' AND rangepos = {$range[$i]["rangepos"]}";

    if (mysqli_query($conn, $sql)) {
      $response->status = true;
      $response->message = 'Aspecto actualizado correctamente';
      $response->aspects = readAspects($sqlReadAspects, $conn);
    } else {
      $response->status = false;
      $response->message = 'Algo salió mal, intentalo nuevamente más tarde';
    }
  }

  echo json_encode($response);
} else {
  $response->status = false;
  $response->message = 'Algo salió mal, intentalo nuevamente más tarde';
  echo json_encode($response);
}
