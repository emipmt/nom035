<?php
  require_once '../conn.php';

  $query = $_GET['query'];
  $sqlReadAspects = "SELECT * FROM aspects ORDER BY id DESC";

  switch ($query) {
    case '*':
		$aspects = readAspects($sqlReadAspects,$conn);

        if (!$aspects) {
          $response->status = false;
          $response->aspects = [];
          echo json_encode($response);
        } else{
          $response->status = true;
          $response->aspects = $aspects;
          echo json_encode($response);
        }

      break;
  }

  function readAspects ($sqlReadAspects,$conn) {
    $resultReadAspects = mysqli_query($conn, $sqlReadAspects);
    if (mysqli_num_rows($resultReadAspects) > 0) {
      $aspects = [];
      while($rowReadAspects = mysqli_fetch_assoc($resultReadAspects)) {
		$ranges = [];

		$sql = "SELECT * from aspectsranges where idaspect = '{$rowReadAspects["id"]}'";
		$result = mysqli_query($conn, $sql);

		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result)){
				array_push($ranges, $row);
			}
		}
		array_push($rowReadAspects, $ranges);
		array_push($aspects,$rowReadAspects);
      }
      return $aspects;
    } else {
      return false;
    }

  }
