<?php

require_once '../conn.php';
require_once 'read.php';

$title = $_POST['title'];
$rangeMin = explode(",", $_POST['rangeMin']);
$rangeMax = explode(",", $_POST['rangeMax']);

$sqlCreateAspects = "INSERT INTO aspects (title) VALUES ('{$title}')";

if (mysqli_query($conn, $sqlCreateAspects)) {

	$idaspect = $conn->insert_id;

	$values = "";
	for ($i = 0; $i < count($rangeMin); $i++) {
		if ($i == count($rangeMin) - 1) {
			$values = $values . "({$idaspect}, {$rangeMin[$i]}, {$rangeMax[$i]}, {$i}) ";
		} else {
			$values = $values . "({$idaspect}, {$rangeMin[$i]}, {$rangeMax[$i]}, {$i}), ";
		}
	}

	$sql = "INSERT INTO aspectsranges (idaspect, min, max, rangepos) VALUES " . $values;

	if (mysqli_query($conn, $sql)) {
		$response->status = true;
		$response->message = 'Aspecto creado correctamente';
		$response->aspects = readAspects($sqlReadAspects, $conn);
		echo json_encode($response);
	} else {
		$response->error = mysqli_error($conn);
		$response->status = false;
		$response->message = 'Algo salió mal, intentalo nuevamente más tarde';
		echo json_encode($response);
	}
} else {
	$response->status = false;
	$response->message = 'Algo salió mal, intentalo nuevamente más tarde';
	echo json_encode($response);
}
