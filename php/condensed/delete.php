<?php

  require_once '../conn.php';
  require_once 'read.php';

  $id = $_POST['id'];

  $sql = "DELETE FROM condensedranges WHERE idcondensed = '{$id}' ";
  if (mysqli_query($conn, $sql)) {
	$sql = "DELETE FROM condensed WHERE id = '{$id}' ";

	if(mysqli_query($conn, $sql)){
		$response->status = true;
		$response->message = 'Condensado eliminado correctamente';
		$response->condensed = readCondensed($sql,$conn);
		echo json_encode($response);
	} else {
		$response->status = false;
		$response->message = 'Algo salió mal, intentalo nuevamente más tarde';
		echo json_encode($response);
	}
  } else {
    $response->status = false;
    $response->message = 'Algo salió mal, intentalo nuevamente más tarde';
    echo json_encode($response);
  }
