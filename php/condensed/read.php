<?php
require_once '../conn.php';

$query = $_GET['query'];
$sqlReadCondensed = "SELECT * FROM condensed ORDER BY id DESC";

switch ($query) {
    case '*':
        $condensed = readCondensed($sqlReadCondensed, $conn);
        if (!$condensed) {
            $response->status = false;
            $response->condensed = [];
            echo json_encode($response);
        } else {
            $response->status = true;
            $response->condensed = $condensed;
            echo json_encode($response);
        }

        break;
}

function readCondensed($sqlReadCondensed, $conn)
{
    $resultReadCondensed = mysqli_query($conn, $sqlReadCondensed);
    if (mysqli_num_rows($resultReadCondensed) > 0) {
        $condensed = [];
        while ($rowReadCondensed = mysqli_fetch_assoc($resultReadCondensed)) {
			$ranges = [];

			$sql = "SELECT * from condensedranges where idcondensed = '{$rowReadCondensed["id"]}'";
			$result = mysqli_query($conn, $sql);

			if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){
					array_push($ranges, $row);
				}
			}
			array_push($rowReadCondensed, $ranges);
            array_push($condensed, $rowReadCondensed);
        }
        return $condensed;
    } else {
        return false;
    }

}
